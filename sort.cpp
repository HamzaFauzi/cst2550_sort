#include "sort.h"

void mergesort(std::vector<int> &list)
{
  if (list.size() > 1) {

    // copy the first half of the list and sort it using mergesort
    std::vector<int> first_half(list.size()/2);
    for (int i = 0; i < first_half.size(); ++i)
      first_half[i] = list[i];
    mergesort(first_half);

    // copy the first half of the list and sort it using mergesort
    std::vector<int> second_half(list.size() - list.size()/2);
    for (int i = 0; i < second_half.size(); ++i)
      second_half[i] = list[first_half.size() + i];
    mergesort(second_half);
    
    // merge the two sorted half lists
    merge(first_half, second_half, list);
  }
}

void merge(std::vector<int> &list1, std::vector<int> &list2,
	   std::vector<int> &destination)
{
  size_t index1 = 0, index2 = 0, dest_index = 0;

  // copy items from sub lists to destination list in sorted order
  // until one of them is empty
  while (index1 < list1.size() && index2 < list2.size()) {
    if (list1[index1] < list2[index2])
      destination[dest_index++] = list1[index1++];
    else
      destination[dest_index++] = list2[index2++];
  }

  // copy remaining items from whichever sub-list still has elements
  // to destination list
  while (index1 < list1.size())
    destination[dest_index++] = list1[index1++];

  while (index2 < list2.size())
    destination[dest_index++] = list2[index2++];
}
